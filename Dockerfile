FROM alpine
RUN apk -Uuv add groff less python py-pip
RUN pip install awscli
RUN apk --purge -v del py-pip
RUN rm /var/cache/apk/*
COPY numeros/ArchivoRandom.py /home/Numeros/ArchivoRandom.py
COPY credentials /root/.aws/
COPY config /root/.aws/
CMD python /home/Numeros/ArchivoRandom.py && aws s3 cp Numeros.txt s3://docktito && /bin/sh
