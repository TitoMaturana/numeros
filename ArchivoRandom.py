from random import randint

def main():
    with open('Numeros.txt', 'w') as file_to_write:
        for i in range(100):
            numero = randint(1,100)
            if i != 99:
                file_to_write.write("%d\n" %numero)
            else:
                file_to_write.write("%d" %numero)

                
if __name__ == "__main__":
    main()    